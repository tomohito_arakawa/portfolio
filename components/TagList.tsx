import React from 'react';
import styles from '../styles/components/TagList.module.scss';

type Props = {
    children: React.ReactNode
};

const TagList: React.FC<Props> = (props) => {
    return (
        <ul className={styles['tag-list']} >
            {props.children}
        </ul>
    )
}
export default TagList;
import React, { useState, useEffect } from 'react';
import styles from '../styles/components/Contact.module.scss';
import Heading from './Heading';
import Select from './Select';
import Option from './Option';
import Input from './Input';
import Button from './Button';
import FormRow from './FormRow';
import Toaster from './Toaster';

type PropsOption = {
    value: string,
    text: string
}

type PropsToaster = {
    status?: string | undefined,
    message?: string | undefined
}

const Contact: React.FC = () => {
    const [options, setOptions] = useState<PropsOption[]>([]);
    const [disabled, setDisabled] = useState<boolean | undefined>();
    const [toaster, setToaster] = useState<PropsToaster>();
    const API_URL = 'https://tomotomo.weblike.jp/wp_headless/wp-json/contact-form-7/v1/contact-forms/24/feedback';
    const handleSubmit = async (e: any) => {
        e.preventDefault();
        setDisabled(true);
        const formElement = e.target;
        const body = new FormData(formElement);
        const res = await fetch(API_URL, {
          method: 'POST',
        //   mode: 'no-cors',
          body: body,
        })
        res.json().then(data => {
            if (data.status === 'mail_sent')
            submitSuccess();
            else if (data.status === 'validation_failed')
            submitError();
        })
    }
    const submitSuccess = () => {
        const context = 'The message has been sent.' + '<br>' + 'Please check the auto-reply email.';
        setToaster({
            status: 'success',
            message: context
        });
        setDisabled(undefined);
        setTimeout(() => {
            setToaster({
                status: '',
                message: context
            });
        }, 3000);
        Array.from(document.querySelectorAll('input')).forEach(
            input => (input.value = '')
        );
        const select: HTMLSelectElement | null = document.querySelector('select');
        if ( select !== null ) {
            select.selectedIndex = 0;
        } else {
            return;
        }
        const textarea: HTMLTextAreaElement | null = document.querySelector('textarea');
        if ( textarea !== null ) {
            textarea.value = '';
        } else {
            return;
        }
    }
    const submitError = () => {
        const context = 'There is a problem with the input contents.' + '<br>' + 'Please check and try again.';
        setToaster({
            status: 'error',
            message: context
        });
        setDisabled(undefined);
        setTimeout(() => {
            setToaster({
                status: '',
                message: context
            });
        }, 3000);
    }
    useEffect(() => {
        setOptions([
            {value: '', text: 'Please choose subject'},
            {value: 'Order / Consult', text: 'Order / Consult'},
            {value: 'Recruit / Scout', text: 'Recruit / Scout'},
            {value: 'Other', text: 'Other'}
        ]);
    }, []);
    return (
        <>
            <Toaster status={toaster?.status} message={toaster?.message} />
            <form id="contact" className={styles.contact} onSubmit={handleSubmit}>
                <Heading type='primary' text='Contact' color='white' />
                <FormRow>
                    <Select name='your-subject'>
                        {options.map((option, index) => (
                            <Option
                                key={index}
                                value={option.value}
                                text={option.text}
                            />
                        ))}
                    </Select>
                </FormRow>
                <FormRow>
                    <Input type='text' name='your-name' placeholder='Name' />
                </FormRow>
                <FormRow>
                    <Input type='email' name='your-email' placeholder='Mail address' />
                </FormRow>
                <FormRow>
                    <Input type='textarea' name='your-message' placeholder='Inquiry contents' />
                </FormRow>
                <FormRow>
                    <Button tag='button' type='submit' text='Send' state={disabled} />
                </FormRow>
            </form>
        </>
    )
}
export default Contact;
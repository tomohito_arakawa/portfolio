import React from 'react';
import styles from '../styles/components/Heading.module.scss';

type Props = {
    type?: string,
    text: string,
    color?: string | null,
}

const Heading: React.FC<Props> = (props) => {
    if ( props.type === 'primary' ) {
        return (
            <h2 className={
                `
                ${styles['heading-primary']} 
                ${props.color === 'white' ? styles['heading-primary--white'] : ''}
                ${props.color === 'gradation' ? styles['heading-primary--gradation'] : ''}
                `
                }>{props.text}
            </h2>
        )
    } else if ( props.type === 'secondary' ) {
        return (
            <h3 className={
                `
                ${styles['heading-secondary']} 
                ${props.color === 'white' ? styles['heading-secondary--white'] : ''}
                ${props.color === 'gradation' ? styles['heading-secondary--gradation'] : ''}
                `
                }>{props.text}
            </h3>
        )
    } else {
        return (
            <h4>{props.text}</h4>
        )
    }
}
export default Heading;
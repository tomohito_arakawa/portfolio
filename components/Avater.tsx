import React from 'react';
import styles from '../styles/components/Avater.module.scss';

type Props = {
    id: number,
    href: string,
    image: string
}

const Avater: React.FC<Props> = (props) => {
    if ( props.href.trim() !== '' ) {
        return (
            <li className={styles.avater} key={props.id}>
                <a href={props.href} className={styles['avater__anchor']} target="_blank" rel="noopener noreferrer">
                    <img src={props.image} className={styles['avater__image']} alt='' />
                </a>
            </li>
        )
    } else {
        return (
            <li className={styles.avater} key={props.id}>
                <img src={props.image} className={styles['avater__image']} alt='' />
            </li>
        )
    }
}
export default Avater;
import React from 'react';
import styles from '../styles/components/Tag.module.scss';

type Props = {
    text: string
}

const Tag: React.FC<Props> = (props) => {
    return (
        <li className={styles.tag}>
            {props.text}
        </li>
    )
}
export default Tag;
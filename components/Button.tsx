import React from 'react';
import Link from 'next/link';
import styles from '../styles/components/Button.module.scss';

type Props = {
    tag: string,
    type?: 'button' | 'submit' | 'reset' | undefined,
    href?: string | undefined,
    state: boolean | undefined,
    text: string
}

const Button: React.FC<Props> = (props) => {
    if ( props.tag === 'button' ) {
        return (
            <button 
                type={props.type} 
                className={styles['button-primary']} 
                disabled={props.state}
            >
                {props.text}
            </button>
        )
    } else if ( props.tag === 'anchor' ) {
        return (
            <a 
                href={props.href} 
                className={styles['button-primary']}
            >
                {props.text}
            </a>
        )
    } else {
        return (
            <button 
                type="button"
                disabled={props.state}
            >
                {props.text}
            </button>
        )
    }
}
export default Button;
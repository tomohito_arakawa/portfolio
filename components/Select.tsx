import React from 'react';
import styles from '../styles/components/Select.module.scss';

type Props = {
    children: React.ReactNode,
    name: string
};

const Select: React.FC<Props> = (props) => {
    return (
        <div className={styles['select-wrap']}>
            <select className={styles.select} name={props.name}>
                {props.children}
            </select>
        </div>
    )
}
export default Select;
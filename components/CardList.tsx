import React from 'react';
import styles from '../styles/components/CardList.module.scss';

type Props = {
    children: React.ReactNode
};

const CardList: React.FC<Props> = (props) => {
    return (
        <ul className={styles['card-list']} >
            {props.children}
        </ul>
    )
}
export default CardList;
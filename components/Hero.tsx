import React from 'react';
import styles from '../styles/components/Hero.module.scss';

type Props = {
    image: string,
    title: string,
    date: string
}

const Hero: React.FC<Props> = (props) => {
    return (
        <figure className={styles.hero}>
            <img src={props.image} alt="" className={styles.hero__image} />
            <figcaption className={styles.hero__caption}>
                <h1 className={styles.hero__title}>{props.title}</h1>
                <span className={styles.hero__author}>
                    <time className={styles.hero__date}>{props.date}</time>
                </span>
            </figcaption>
        </figure>
    )
}
export default Hero;
import React from 'react';
import styles from '../styles/components/Input.module.scss';

type Props = {
    type: string,
    name: string,
    placeholder: string
}

const Input: React.FC<Props> = (props) => {
    if ( props.type.toUpperCase() === 'EMAIL' ) {
        return (
            <input 
                type="email" 
                name={props.name} 
                placeholder={props.placeholder} 
                autoComplete="email" 
                className={styles.input} 
            />
        )
    } else if ( props.type.toUpperCase() === 'TEXT' ) {
        return (
            <input 
                type="text" 
                name={props.name} 
                placeholder={props.placeholder} 
                autoComplete="name" 
                className={styles.input} 
            />
        )
    } else if ( props.type.toUpperCase() === 'TEXTAREA' ) {
        return (
            <textarea 
                name={props.name} 
                placeholder={props.placeholder} 
                className={styles.textarea}></textarea>
        )
    } else {
        return (
            <input 
                type={props.type} 
                name={props.name} 
                placeholder={props.placeholder} 
                autoComplete="on" 
                className={styles.input} 
            />
        )
    }
}
export default Input;
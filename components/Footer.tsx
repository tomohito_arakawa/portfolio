import React from 'react';
import styles from '../styles/components/Footer.module.scss';

const Footer: React.FC = () => {
    return (
        <footer className={styles.footer}>
            ©2022 Tomohito Arakawa
        </footer>
    )
}
export default Footer;
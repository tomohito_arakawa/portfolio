import React from 'react';
import styles from '../styles/components/Toaster.module.scss';

type Props = {
    status?: string | undefined,
    message?: string | undefined
}

const Toaster: React.FC<Props> = (props) => {
    if (props.message !== undefined) {
        return (
            <div
                className={
                    `
                    ${styles['toaster']}
                    ${props.status === 'success' ? styles['toaster--success'] : ''}
                    ${props.status === 'error' ? styles['toaster--error'] : ''}
                    `
                }
                dangerouslySetInnerHTML={{__html: props.message}}
            />
        )
    } else {
        return(
            <div className={styles['toaster']} />
        )
    }
}
export default Toaster;
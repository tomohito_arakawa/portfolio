import React, { useState, useEffect } from 'react';
import styles from '../styles/components/Work.module.scss';
import CardList from './CardList';
import Card from './Card';
import CardLoad from './CardLoad';

const Work = () => {
    const [loading, setLoading] = useState<boolean>(true);
    const [cards, setCards] = useState([]);
    useEffect(() => {
        fetch('https://tomotomo.weblike.jp/wp_headless/wp-json/wp/v2/portfolio?per_page=100&_embed')
        .then(response => response.json())
        .then(data => {
            setCards(data)
        })
        .then(() => {
            setLoading(false)
        })
        .catch(console.error);
    }, []);
    return (
        <section id="works" className={styles.work}>
            <CardList>
                { loading === true && ( 
                    [1,2,3,4,5,6,7,8].map((value, index) => <CardLoad key={index} />)
                )}
                {cards.map((card, index) => (
                    <Card
                        key={index}
                        slug={card['slug']}
                        image={card['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['full']['source_url']}
                        title={card['title']['rendered']}
                        date={card['date']}
                    />
                ))}
            </CardList>
        </section>
    )
}
export default Work;
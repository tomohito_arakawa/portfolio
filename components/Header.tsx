import React from 'react';
import Link from 'next/link'
import styles from '../styles/components/Header.module.scss';

const Header: React.FC = () => {
    return (
        <header className={styles.header}>
            <div className={styles.header__inner}>
                <Link href="/" className={styles['header__anchor']}>
                    <img src="/portfolio/avater.svg" alt="" className={styles['header__avater']} />
                </Link>
                <ul className={styles['header__list']}>
                    <li className={styles['header__item']}>
                        <Link href="/#works" className={styles['header__anchor']}>
                            WORKS
                        </Link>
                    </li>
                    <li className={styles['header__item']}>
                        <Link href="/#about" className={styles['header__anchor']}>
                            ABOUT
                        </Link>
                    </li>
                    <li className={styles['header__item']}>
                        <Link href="/#contact" className={styles['header__anchor']}>
                            CONTACT
                        </Link>
                    </li>
                </ul>
            </div>
        </header>
    )
}
export default Header;
import React from 'react';

type Props = {
    value: string,
    text: string
}

const Option: React.FC<Props> = (props) => {
    return (
        <option value={props.value}>
            {props.text}
        </option>
    )
}
export default Option;
import React from 'react';
import styles from '../styles/components/CardLoad.module.scss';

const CardLoad: React.FC = () => {
    return (
        <li className={styles['card-load']}>
            <div className={styles['card-load__thumb']}>
                <img src="/portfolio/spacer.png" alt="" className={styles['card-load__image']} />
                <div className={styles['card-load__desc']}>
                    <h3 className={styles['card-load__title']}></h3>
                    <div className={styles['card-load__content']} />
                    <div className={styles['card-load__author']}>
                        <span className={styles['card-load__avater']} />
                        <span className={styles['card-load__name']}></span>
                        <time className={styles['card-load__date']}></time>
                    </div>
                </div>
            </div>
        </li>
    )
}
export default CardLoad;
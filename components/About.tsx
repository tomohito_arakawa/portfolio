import React, { useState, useEffect } from 'react';
import Heading from './Heading';
import TagList from './TagList';
import Tag from './Tag';

import styles from '../styles/components/About.module.scss';

type Props = {
    text: string
}

const About: React.FC = () => {
    const [tags, setTags] = useState<Props[]>([]);
    useEffect(() => {
        setTags([
            {text: 'HTML5(Semantic)'},
            {text: 'CSS / SASS(SCSS)'},
            {text: 'BEM / OOCSS / FLOCSS'},
            {text: 'Javascript(ES6)'},
            {text: 'React × Next.js'},
            {text: 'Svelte'},
            {text: 'Webpack'},
            {text: 'Git'},
            {text: 'Wordpress'},
            {text: 'Atomic design'},
            {text: 'Responsive design'},
            {text: 'UI design'},
            {text: 'Web cording education'}
        ]);
    }, []);
    return (
        <section id="about" className={styles.about}>
            <Heading type='primary' text='Skill Set' />
            <Heading type='secondary' text="Web Frontend Developer" color='gradation' />
            <TagList>
                {tags.map((tag, index) => (
                    <Tag
                        key={index}
                        text={tag.text}
                    />
                ))}
            </TagList>
        </section>
    )
}
export default About;
import React from 'react';
import Link from 'next/link'
import styles from '../styles/components/Card.module.scss';

type Props = {
    slug: string,
    image?: string,
    title: string,
    date: string
}

const Card: React.FC<Props> = (props) => {
    return (
        <li className={styles.card}>
            <figure className={styles['card__thumb']}>
                <img src={props.image} className={styles['card__image']} alt='' />
                <figcaption className={styles['card__desc']}>
                    <Link href={`/posts/${props.slug}`} className={styles['card__anchor']}>
                        <h3 className={styles['card__title']}>{props.title}</h3>
                        <div className={styles['card__detail']}>
                            <time className={styles['card__date']}>{props.date.slice(0,10).split('-').join('.')}</time>
                            <span className={styles['card__more']}>MORE</span>
                        </div>
                    </Link>
                </figcaption>
            </figure>
        </li>
    )
}
export default Card;
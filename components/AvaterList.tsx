import React from 'react';
import styles from '../styles/components/AvaterList.module.scss';

type Props = {
    children: React.ReactNode
};

const AvaterList: React.FC<Props> = (props) => {
    return (
        <ul className={styles['avater-list']} >
            {props.children}
        </ul>
    )
}
export default AvaterList;
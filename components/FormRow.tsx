import React from 'react';
import styles from '../styles/components/FormRow.module.scss';

type Props = {
    children: React.ReactNode;
};

const FormRow: React.FC<Props> = (props) => {
    return (
        <div className={styles['form-row']}>
            {props.children}
        </div>
    )
}
export default FormRow;
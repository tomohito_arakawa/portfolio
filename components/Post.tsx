import React from 'react';
import Head from 'next/head';
import Hero from './Hero';

import styles from '../styles/components/Post.module.scss';

type Props = {
    image: string,
    title: string,
    content: string,
    date: string,
}

const Post: React.FC<Props> = (props) => {
    return (
        <>

            <Head>
                <meta name="apple-mobile-web-app-capable" content="yes" />
                <title>{props.title} | PORTFOLIO | TOMOHITO ARAKAWA</title>
                <meta name="description" content="Tomohito Arakawa is web frontend developer. I have an 8 year career in this job. I'm from Japan. I'm living Thailand now." />
                <meta content="#ff66a3" name="theme-color" />
                <meta content="#ff66a3" name="msapplication-TileColor" />
                <meta content="#ff66a3" name="theme-color" />
                <link rel="stylesheet" href="https://use.typekit.net/wyy2kzw.css" />
                <link rel="icon" href="/portfolio/favicon-16×16.ico" />
                <link rel="icon" href="/portfolio/favicon-32×32.ico" />
                <link rel="apple-touch-icon" sizes="180x180" href="/portfolio/apple-touch-icon.png" />
            </Head>

            <Hero
                image={props.image}
                title={props.title}
                date={props.date.slice(0,10).split('-').join('.')}
            />

            <section className={styles.post} dangerouslySetInnerHTML={{__html: props.content}} />

        </>
    )
}
export default Post;
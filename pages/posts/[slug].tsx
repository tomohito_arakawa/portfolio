import React, { useState, useEffect } from 'react';
import {useRouter} from 'next/router';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Post from '../../components/Post';


const Slug = () => {

   //routerオブジェクトを用意
   const router = useRouter();

   const slug = router.query.slug;
   console.log(slug);

   const [contents, setContetnts] = useState([]);
   useEffect(() => {
       fetch(`https://tomotomo.weblike.jp/wp_headless/wp-json/wp/v2/portfolio?slug=${slug}&_embed`)
       .then(response => response.json())
       .then(data => {
         setContetnts(data)
        })
       .catch(console.error);
   }, []);

   return (
    <>

      <Header />

      {contents.map((content, index) => (
          <Post
              key={index}
              image={content['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['full']['source_url']}
              title={content['title']['rendered']}
              content={content['content']['rendered']}
              date={content['date']}
          />
      ))}

      <Footer />

    </>
   );
}

// This function gets called at build time
export async function getStaticPaths() {
  // Call an external API endpoint to get posts
  const res = await fetch('https://tomotomo.weblike.jp/wp_headless/wp-json/wp/v2/portfolio?per_page=100&_embed')
  const posts = await res.json()

  // Get the paths we want to pre-render based on posts
  const paths = posts.map((post: { slug: string; }) => `/posts/${post.slug}`)

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false }
}

// This also gets called at build time
export async function getStaticProps() {
  // params contains the post `id`.
  // If the route is like /posts/1, then params.id is 1
  const res = await fetch(`https://tomotomo.weblike.jp/wp_headless/wp-json/wp/v2/portfolio?per_page=100&_embed`)
  const post = await res.json()

  // Pass post data to the page via props
  return { props: { post } }
}

export default Slug
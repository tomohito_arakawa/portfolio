import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import Work from '../components/Work';
import About from '../components/About';
import Contact from '../components/Contact';
import Footer from '../components/Footer';

const Home = () => {
  // const [height, setHeight] = useState<number | undefined>();
  // useEffect(() => {
  // }, []);
  return (
    <>

      <Head>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>PORTFOLIO | TOMOHITO ARAKAWA</title>
        <meta name="description" content="Tomohito Arakawa is web frontend developer. I have an 8 year career in this job. I'm from Japan. I'm living Thailand now." />
        <meta content="#ff66a3" name="theme-color" />
        <meta content="#ff66a3" name="msapplication-TileColor" />
        <meta content="#ff66a3" name="theme-color" />
        <link rel="stylesheet" href="https://use.typekit.net/wyy2kzw.css" />
        <link rel="icon" href="/portfolio/favicon-16×16.ico" />
        <link rel="icon" href="/portfolio/favicon-32×32.ico" />
        <link rel="apple-touch-icon" sizes="180x180" href="/portfolio/apple-touch-icon.png" />
      </Head>

      <Work />

      <About />

      <Contact />

      <Footer />

    </>
  )
}
export default Home;

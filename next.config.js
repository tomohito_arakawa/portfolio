/** @type {import('next').NextConfig} */
const path = require('path')
const nextConfig = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  basePath: '/portfolio',
  assetPrefix: '/portfolio',
  experimental: {
    appDir: true,
  },
};

module.exports = nextConfig;
